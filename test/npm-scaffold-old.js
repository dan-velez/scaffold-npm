module.exports = {
    npm_scaffold,
	run_npm_init,
	create_directories,
	create_readme,
	create_starter_files,
	create_binary,
};

function scaffold_module(err) {
    try {
        run_npm_init(()=> {

        });
    } catch(e) {

    };
}

run_npm_init();
function run_npm_init(callb) {
    /* Runs 'npm init', creates inital directory. */
	let npm = spawn('npm', ['init']);
	npm.stdout.on('data', (data)=> {
		console.log(data.toString('utf-8'));
	});
	npm.stderr.on('data', (data)=> {
		console.log(data)
		process.exit();
	});
	// npm.on('close', callb);
	/* Package.json should be created, require it for metadata. */
    /*
	try {
		const pack = require('./package.json');
	} catch(e) {
		console.log("Package could not be created successfuly...");
		process.exit();
	}
    */
}

function create_directories() {
	fs.mkdirSync("bin/");
	fs.mkdirSync("lib/");
	fs.mkdirSync("test/");
}

function create_starter_files() {
    // gitignore
    // npm ignore
    // lib/index.js
    // test/index.js
    // bin/package-name + '#/usr/env/node'
}

function create_readme() {
    /* Create a readme with the title and description.*/
	fs.writeFileSync("README.md", `
	# ${pack['name']}
	`)
}

function create_binary() {
	/* Manually edit the package file. and create a file in bin/ */
}

function create_git_files() {
    /* Run git init. */
}
