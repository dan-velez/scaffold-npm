#!/usr/bin/env node
/* Scaffold the project folder and prep for npm and git.
 *
 * Read config
 * Get info from prompt
 * Create dirs
 * Create js code: bin/pkg_name, lib/index.js, test/index.js
 * Create pkg file
 * Create ignore files
 * Create readme
 * Run git init
 */

const readline = require('readline');
const fs = require('fs');
const path = require('path');
const {exec} = require('child_process');

let config_path = process.env.HOME + "/.npm-scaffold.config.json";
let user_config = {
    /* Parsed user configuration options. */
    "additional-directories": [],
    "additional-files": []
};

let package_info = {
    /* Used to construct package.json as well as how to scaffold
     * the project. For internal use.
     */
    "name": "my-test-package",
    "description": "A test package."
};

module.exports = { main };

function main() {
    /* CLI tool. Initiate an interactive prompt to scaffold the users project. */
    let pkg = require("../package.json");
    console.log("npm scaffold", pkg['version']);
    console.log("Follow the prompts to create an npm package directory ready for development.");

    prompt_info()
    .then(resp => {
        return read_config();
    })
    .then(resp => {
        return create_dir_structure();
    })
    .then(resp => {
        return create_code_files();
    })
    .then(resp => {
        return create_package_file();
    })
    .then(resp => {
        return create_ignore_files();
    })
    .then(resp => {
        return create_readme(); 
    })
    .then(resp => {
        return git_init();
    })
    .catch(err => {
        console.log("Error! Could not create your project.", err);
    });
}

function create_readme() {
    let fpath = package_info['name'] + "/";
    let readme_content = `#${package_info['name']}#\n`+
                         `${package_info['description']}\n`+
                         `##Installation##\n`+
                         `##Usage##\n`
    return new Promise((resolve, reject)=> {
        fs.writeFile(fpath+"/README.md", readme_content, (err)=> {
            if(err) {
                reject(err);
            }
            else {
                resolve();
            }
        });
    });
}

function git_init() {
    /* Initialize the directory as a git repo. */
    let cmd = `cd ${package_info['name']} && git init`;
    return new Promise((resolve, reject)=> {
        exec(cmd, (err, stdout, stderr)=> {
            if(err) {
                reject(err);
            }
            else {
                console.log(stdout);
                resolve();
            }
        });
    });
}

function create_ignore_files() {
    /* Create .ignore files for npm and git. */
    let fpath = package_info['name'] + "/";
    let gitignore = ".*.swp\n"+
                    "node_modules/\n"+
                    "bin/"
    let npmignore = ".*.swp\n"+
                    "node_modules/\n"+
                    "lib/"
    return new Promise((resolve, reject)=> {
        fs.writeFile(fpath+".gitignore", gitignore, (err)=> {
            if(err) {
                reject(err);
            }
            else {
                fs.writeFile(fpath+".npmignore", npmignore, (err)=> {
                    if(err) {
                        reject(err);
                    }
                    else {
                        resolve();
                    }
                });
            }
        });
    });
}

function create_package_file() {
    /* Create the package file for the project.
     * Read in template, replace info, save to project folder.
     */
    let package_template_path = path.resolve("lib/package-template.json");
    console.log("Reading package template:["+package_template_path+"]");
    return new Promise((resolve, reject)=> {
        fs.readFile(package_template_path, (err, dat)=> {
            if(err) {
                reject(err);
            }
            else {
                let package_template_parsed = JSON.parse(dat.toString('utf-8'));
                console.log("Package template read in.");
                /* Fill in the data. */
                package_template_parsed['name'] = package_info['name'];
                package_template_parsed['description'] = package_info['description'];
                package_template_parsed['main'] = package_info['main'];
                package_template_parsed['directories'] = {
                    "lib": "lib/",
                    "bin": "bin/",
                    "test": "test/"
                };
                package_template_parsed['repository']['url'] = package_info['git-url'];
                package_template_parsed['keywords'] = package_info['keywords'];
                package_template_parsed['bin'] = {};
                package_template_parsed['bin'][package_info['name']] = "bin/"+ package_info['name'].replace(/\-/g, "_");
                package_template_parsed['author'] = package_info['author'];
                package_template_parsed['license'] = package_info['license'];
                console.log("Writing package file: ", JSON.stringify(package_template_parsed, null, 4));
                fs.writeFile(package_info['name'] + "/package.json", 
                 JSON.stringify(package_template_parsed, null, 4), (err)=>{
                    if(err){
                        reject(err);
                    }
                    else {
                        create_package_lock().then(resolve);
                    }
                })
            }
        });
    });
}

function create_package_lock() {
    /* Create additional lock file that is created by npm init. */
    let fpath = package_info['name']+"/package-lock.json";
    let lock = {
      "name": package_info['name'],
      "version": "1.0.0",
      "lockfileVersion": 1
    };
    return new Promise((resolve, reject)=> {
        fs.writeFile(fpath, JSON.stringify(lock, null, 4), (err)=> {
            if(err) {
                reject(err);
            }
            else {
                resolve();
            }
        });
    });
}

function read_config() {
    /* Read and parse the config file into memory. */
    return new Promise((resolve, reject)=> {
        create_config()
        .catch((err)=> reject(err))
        .then((resp)=> {
            /* Read in the config. */
            fs.readFile(config_path, (err, text)=> {
                if(err) reject(err);
                else {
                    user_config = JSON.parse(text.toString('utf-8'));
                    console.log("Config read in", user_config);
                    resolve();
                }
            });
        })
    });
}

function create_config() {
    /* Create the file if it does not exist. */
    let default_config = {
        "create-test": true,
        "create-bin": true,
        "run-git-init": true,
        "additional-directories": [],
        "additional-files": []
    };
    return new Promise((resolve, reject)=> {
        fs.open(config_path, "r", (err, fd)=> {
            if(err && err.code == "ENOENT") {
                /* Does not exist. Create it. */
                fs.writeFile(config_path, JSON.stringify(default_config, null, 4), (err)=> {
                    if(err) reject(err);
                    else {
                        resolve();
                    }
                });
            } else if(err) {
                console.log("Could not create or read config!");
                reject(err);
            } else {
                resolve(); /* Already exists... */
            }
        });
    });
}

function create_dir_structure() {
    /* Create the directory structure for the npm package. */
    console.log("Creating directory structure...");
    let dirname = package_info['name'];
    let dirs = ["lib/", "bin/", "test/"].concat(user_config['additional-directories']);
    let counter = 0;
    return new Promise((resolve, reject)=> {
        /* Create main directory first. */
        fs.mkdir(dirname, (err)=> {
            if(err) reject(err);
            else {
                /* Create all the directories in the dirs array. */
                dirs.map((subdirname)=> {
                    console.log(`Creating dir: [${package_info['name']+"/"+subdirname}]`);
                    fs.mkdir(dirname +"/"+ subdirname, (err)=> {
                        if(err) reject(err);
                        else {
                            /* Wait till all directories are created. */
                            ++counter;
                            if(counter == dirs.length) {
                                resolve();
                            }
                        }
                    });
                });
            }
        })
    });
}

function prompt_info() {
    /* Prompt the user for info about the package. 
     * Get whatever is in an actual package.json file.
     */
    return new Promise((resolve, reject)=> {
        prompt_user("Package name: ")
        .then(resp => {
            package_info['name'] = resp;
            return prompt_user("Description: ");
        })
        .then(resp => {
            package_info['description'] = resp;
            return prompt_user("main (default lib/index.js): ");
        })
        .then(resp => {
            package_info['main'] = resp || "lib/index.js";
            return prompt_user("git url: ");
        })
        .then(resp => {
            package_info['git-url'] = resp;
            return prompt_user("keywords (seperate by commas): ");
        })
        .then(resp => {
            package_info['keywords'] = resp.split(",").map(x => { return x.trim(); });
            return prompt_user("author: ");
        })
        .then(resp => {
            package_info['author'] = resp;
            return prompt_user("license (default ISC): ");
        })
        .then(resp => {
            package_info['license'] = resp || "ISC";
            return;
        })
        .then(resp => {
            resolve();
        });
    });
}

function prompt_user(text) {
    /* Get text input from user. Can be chained to 
     * create multiple prompts.
     */
    return new Promise((resolve, reject)=> {
        const rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });
        rl.question(text, (resp)=> {
            resolve(resp);
            rl.close();
        });
    });
}

function create_code_files() {
    /* Create js code: bin/pkg_name, lib/index.js, test/index.js. */
    let package_name_clean = package_info['name'].replace(/\-/g, "_");
    let code_files = [
        {
            "path": "bin/"+package_info['name'],
            "contents": `#!usr/env/node\n` +
                        `const ${package_name_clean} = require('../lib/');\n` +
                        `${package_name_clean}.main();\n`
        },
        {
            "path": "lib/index.js",
            "contents": `module.exports = require('./${package_info['name']}')`
        },
        {
            "path": `lib/${package_info['name']}.js`,
            "contents": "/* "+package_info['name']+" */\n" +
                        "function main() {\n" +
                         "\n" +
                         "}"
        },
        {
            "path": "test/index.js",
            "contents": `const ${package_name_clean} = require('../lib/');\n` +
                        `${package_name_clean}.main();`
        },
    ].concat(user_config['additional-files']);
    return new Promise((resolve, reject)=> {
        let counter = 0;
        code_files.map((code_file)=> {
            fs.writeFile(package_info['name'] + "/" + code_file.path, code_file.contents, (err)=> {
                console.log("Wrote file: " + package_info['name']+"/"+code_file.path.trim());
                if(err) return reject(err);
                else {
                    ++counter;
                    if(counter == code_files.length) {
                        resolve();
                    }
                } 
            });
        });
    });
}
