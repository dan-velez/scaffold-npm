# scaffold-npm #
Create a project folder ready to start development for npm. Walks user through an interactive prompt. It is like an improved version of `npm init`.

## Installation ##
`npm ig --save scaffold-npm`

## Usage ##
```
$ scaffold-npm
```
This will take you through an interactive session which gathers info for the package.

## Default Structure ##
```
my-package/
    bin/
        my-package
    lib/
        index.js
        my-package.js

    test/
        index.js
    .git
    .gitignore
    .npmignore
    package.json
    package-lock.json.
    README.md
```

## Configuration ##
The config file is created at `~/.npm-scaffold.json`. The fields are listed below.

___

**create-test**
> **Boolean** Determines wether the test/ directory is created.

**create-bin**
> **Boolean** Determines wether the bin/ directory is created.

**run-git-init**
> **Boolean** Determines wether to initiate the package as a git repo as well.

**additional-directories**
> **Array** A list of relative paths for extra directories to be created along with scaffold.

**additional-files**
> **Array** A list of relative paths for extra files to be created along with scaffold. The files are in the format
> { path: "relative-path", contents: "contents-of-file" }

